#include <QtQml>
#include <QtQml/QQmlContext>

#include "plugin.h"
#include "sprint.h"
#include "applications.h"
#include "app.h"
#include "iconpacks.h"
#include "iconpack.h"

void SprintPlugin::registerTypes(const char *uri) {
    //@uri Sprint
    qmlRegisterSingletonType<Sprint>(uri, 1, 0, "Sprint", [](QQmlEngine*, QJSEngine*) -> QObject* { return new Sprint; });
    qmlRegisterSingletonType<Applications>(uri, 1, 0, "Applications", [](QQmlEngine*, QJSEngine*) -> QObject* { return new Applications; });
    qmlRegisterType<App>(uri, 1, 0, "App");
    qmlRegisterSingletonType<IconPacks>(uri, 1, 0, "IconPacks", [](QQmlEngine*, QJSEngine*) -> QObject* { return new IconPacks; });
    qmlRegisterType<IconPack>(uri, 1, 0, "IconPack");

}
