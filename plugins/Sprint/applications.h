#ifndef APPLICATIONS_H
#define APPLICATIONS_H

#define DESKTOP_FILES_FOLDER_USER QDir::homePath() + "/.local/share/applications"
#define DESKTOP_FILES_FOLDER_SYSTEM "/usr/share/applications"

#include <QObject>
#include <QDir>
#include <QQmlListProperty>
#include <QProcess>
#include <QSettings>

#include "app.h"

class Applications: public QObject {
    Q_OBJECT

    Q_PROPERTY(QQmlListProperty<App> list READ list NOTIFY listChanged)
    Q_PROPERTY(QQmlListProperty<App> filteredList READ filteredList NOTIFY filteredListChanged)
    Q_PROPERTY(QQmlListProperty<App> launcherList READ launcherList NOTIFY launcherListChanged)
    Q_PROPERTY(QQmlListProperty<App> favoriteList READ favoriteList NOTIFY favoriteListChanged)
    Q_PROPERTY(bool isLauncherEmpty READ isLauncherEmpty NOTIFY launcherListChanged)

public:
    Applications();
    ~Applications() = default;

    Q_INVOKABLE void refresh();
    Q_INVOKABLE void filter(const QString &searchTerm);
    Q_INVOKABLE void clearFilter();
    Q_INVOKABLE App* get(const QString &appId);
    Q_INVOKABLE void uninstall(const QString &packageName, const QString &version);
    Q_INVOKABLE void initialize(const int numLauncher, const int numFavorites);
    Q_INVOKABLE void insertLauncherApp(const int index, const QString appId);
    Q_INVOKABLE void insertFavoriteApp(const int index, const QString appId);

    QQmlListProperty<App> list();
    QQmlListProperty<App> filteredList();
    QQmlListProperty<App> launcherList();
    QQmlListProperty<App> favoriteList();
    bool isLauncherEmpty() const;

Q_SIGNALS:
    void listChanged();
    void launcherListChanged();
    void favoriteListChanged();
    void filteredListChanged();
    void uninstallFinished(bool ok);

public Q_SLOTS:
    void uninstallProcessFinished(int exitCode, QProcess::ExitStatus exitStatus);

private:
    Q_DISABLE_COPY(Applications)

    void loadDesktopFiles(const QString &path);
    void loadApps(const bool launcher);

    QList<App *> m_applications;
    QList<App *> m_filteredApplications;
    QList<App *> m_launcherApplications;
    QList<App *> m_favoriteApplications;
    QString m_searchTerm;
    QProcess m_uninstallProcess;
    QSettings m_settings;
    bool m_appsLoaded = false;
    int m_numLauncher = 0;
    int m_numFavorites = 0;
};

#endif
