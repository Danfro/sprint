import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3

import Sprint 1.0
import "Components"

Page {
    property var settings

    header: PageHeader {
        id: header
        title: i18n.tr('Settings')
    }

    ColumnLayout {
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
        }

        spacing: units.gu(1)

        Label {
            Layout.margins: units.gu(1)

            text: i18n.tr('Icon Pack')
        }

        GridLayout {
            columns: 4
            columnSpacing: units.gu(1)
            rowSpacing: units.gu(1)

            Repeater {
                model: IconPacks.list

                delegate: Rectangle {
                    Layout.fillWidth: true
                    Layout.preferredHeight: width + units.gu(2)
                    Layout.maximumWidth: units.gu(10)

                    radius: 10
                    color: (settings.iconPackId == model.id) ? theme.palette.highlighted.selection : 'transparent'

                    Launcher {
                        anchors.fill: parent

                        app: model
                        color: 'black'

                        onLaunch: pageStack.push(Qt.resolvedUrl("IconPackPage.qml"), {
                            settings: settings,
                            iconPack: model
                        })
                    }
                }
            }

            Rectangle {
                Layout.fillWidth: true
                Layout.preferredHeight: width + units.gu(2)
                Layout.maximumWidth: units.gu(10)

                radius: 10
                color: (!settings.iconPackId) ? theme.palette.highlighted.selection : 'transparent'

                Launcher {
                    anchors.fill: parent

                    app: QtObject {
                        property string id: 'default'
                        property string icon: Qt.resolvedUrl('../assets/default.svg')
                        property string name: 'Default'
                        property string title: 'Default'
                        property string author: 'UBports'
                        property string maintainer: 'UBports'
                        property string description: 'Default icons provided by each app'
                        property string preview: ''
                    }
                    color: 'black'

                    onLaunch: pageStack.push(Qt.resolvedUrl('IconPackPage.qml'), {
                        settings: settings,
                        iconPack: app
                    })
                }
            }
        }

        Button {
            Layout.margins: units.gu(1)

            text: i18n.tr('Get more icon packs on the OpenStore')

            // TODO open in the openstore app
            onClicked: Qt.openUrlExternally('https://open-store.io/?search=icon-pack')
        }

        Item {
            id: spacer

            Layout.fillHeight: true
        }
    }
}
